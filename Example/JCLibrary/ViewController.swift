//
//  ViewController.swift
//  JCLibrary
//
//  Created by shivalpandya@yahoo.co.in on 06/05/2019.
//  Copyright (c) 2019 shivalpandya@yahoo.co.in. All rights reserved.
//

import UIKit
import JCLibrary

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let section = TestSection().testSection()
        print(section)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

