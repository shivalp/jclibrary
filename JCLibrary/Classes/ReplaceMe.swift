
import Eureka
import ImageRow

public class TestSection {
    
    public init() {}
    
    public func testSection() -> Section {
        let section = Section()
        section.tag = "Test"
        return section
    }
}

