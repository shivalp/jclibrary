
Pod::Spec.new do |s|
  s.name             = 'JCLibrary'
  s.version          = '0.0.1'
  s.summary          = 'Create private pod for JCLibrary.'

  s.description      = 'This podsec file will be hosted on private repo to be used in all the apps.'

  s.homepage         = 'https://shivalp@bitbucket.org/shivalp/jclibrary'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'shivalpandya@yahoo.co.in' => 'shivalpandya@yahoo.co.in' }
  s.source           = { :git => 'https://shivalp@bitbucket.org/shivalp/jclibrary.git', :tag => s.version.to_s }

  s.ios.deployment_target = '11.0'
  s.swift_version       = '4.2'
  s.source_files = 'JCLibrary/Classes/**/*'
  
  # s.resource_bundles = {
  #   'JCLibrary' => ['JCLibrary/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'Eureka'
    s.dependency 'ImageRow'
end
