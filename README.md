# JCLibrary

[![CI Status](https://img.shields.io/travis/shivalpandya@yahoo.co.in/JCLibrary.svg?style=flat)](https://travis-ci.org/shivalpandya@yahoo.co.in/JCLibrary)
[![Version](https://img.shields.io/cocoapods/v/JCLibrary.svg?style=flat)](https://cocoapods.org/pods/JCLibrary)
[![License](https://img.shields.io/cocoapods/l/JCLibrary.svg?style=flat)](https://cocoapods.org/pods/JCLibrary)
[![Platform](https://img.shields.io/cocoapods/p/JCLibrary.svg?style=flat)](https://cocoapods.org/pods/JCLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JCLibrary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JCLibrary'
```

## Author

shivalpandya@yahoo.co.in, ts-shival.pandya@rakuten.com

## License

JCLibrary is available under the MIT license. See the LICENSE file for more info.
